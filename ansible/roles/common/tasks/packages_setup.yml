---
# Debian
- name: Install system packages (Ubuntu)
  apt:
    name: "{{ system_packages_deb }}"
    update_cache: true
  register: result
  retries: 20
  delay: 5
  until: result is success
  when: ansible_facts['os_family'] == "Debian"

# RHEL
- name: Setup EPEL Repo (RHEL)
  block:
    - name: Setup EPEL GPG key (RHEL)
      rpm_key:
        key: "https://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-{{ ansible_facts['distribution_major_version'] }}"
        state: present
      register: result
      retries: 5
      delay: 5
      until: result is success

    - name: Setup EPEL Repo (RHEL)
      yum:
        name: "https://dl.fedoraproject.org/pub/epel/epel-release-latest-{{ ansible_facts['distribution_major_version'] }}.noarch.rpm"
      register: result
      retries: 5
      delay: 5
      until: result is success
  when:
    - ansible_facts['os_family'] == 'RedHat'
    - ansible_facts['distribution_major_version'] == '8'

- name: Setup EPEL Repo (RHEL - Amazon Linux 2)
  command: amazon-linux-extras install epel -y
  when:
    - ansible_facts['distribution'] == 'Amazon'
    - ansible_facts['distribution_major_version'] == '2'

- name: Install system packages (RHEL)
  yum:
    name: "{{ system_packages_rhel }}"
    update_cache: true
  register: result
  retries: 20
  delay: 5
  until: result is success
  when: ansible_facts['os_family'] == 'RedHat'

# Python
- name: Install python packages
  pip:
    name: "{{ python_packages }}"

# Automated Security Upgrades
- name: Configure Automatic Security Upgrades (Ubuntu)
  include_role:
    name: jnv.unattended-upgrades
  when:
    - system_packages_auto_security_upgrade
    - ansible_facts['os_family'] == "Debian"

- name: Configure Automatic Security Upgrades (RHEL 8)
  block:
    - name: Install dnf-automatic package
      package:
        name: dnf-automatic
        state: present

    - name: Configure dnf-automatic package
      replace:
        path: /etc/dnf/automatic.conf
        regexp: "{{ item.search }}"
        replace: "{{ item.replace }}"
      loop:
        - { search: 'apply_updates = \w+$', replace: 'apply_updates = yes' }
        - { search: 'upgrade_type = \w+$', replace: 'upgrade_type = security' }

    - name: Disable old dnf-automatic service if enabled
      service:
        name: dnf-automatic-install.timer
        enabled: false

    - name: Enable and start dnf-automatic service
      service:
        name: dnf-automatic.timer
        enabled: true
        state: started
  when:
    - system_packages_auto_security_upgrade
    - ansible_facts['os_family'] == 'RedHat'
    - ansible_facts['distribution_major_version'] == '8'

- name: Configure Automatic Security Upgrades (RHEL - Amazon Linux 2)
  block:
    - name: Install yum-cron package
      package:
        name: yum-cron
        state: present

    - name: Configure yum-cron package
      replace:
        path: /etc/yum/yum-cron.conf
        regexp: "{{ item.search }}"
        replace: "{{ item.replace }}"
      loop:
        - { search: 'apply_updates = \w+$', replace: 'apply_updates = yes' }
        - { search: 'update_cmd = \w+$', replace: 'update_cmd = security' }

    - name: Enable and start yum-cron service
      service:
        name: yum-cron
        enabled: true
        state: started
  when:
    - system_packages_auto_security_upgrade
    - ansible_facts['distribution'] == 'Amazon'
    - ansible_facts['distribution_major_version'] == '2'
