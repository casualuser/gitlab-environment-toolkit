---
- name: Setup GitLab config file
  template:
    src: templates/sidekiq.gitlab.rb.j2
    dest: /etc/gitlab/gitlab.rb
  tags: reconfigure

- name: Check if custom config exists
  stat:
    path: "{{ sidekiq_custom_config_file }}"
  delegate_to: localhost
  become: false
  tags: reconfigure
  register: sidekiq_custom_config_file_path

- name: Setup Custom Config
  template:
    src: "{{ sidekiq_custom_config_file }}"
    dest: "/etc/gitlab/gitlab.sidekiq.custom.rb"
    mode: 0644
  tags: reconfigure
  when: sidekiq_custom_config_file_path.stat.exists

- name: Copy over any Custom Files
  copy:
    src: "{{ item.src_path }}"
    dest: "{{ item.dest_path }}"
    mode: "{{ item.mode if item.mode is defined else 'preserve' }}"
  loop: "{{ sidekiq_custom_files_paths }}"
  tags: reconfigure

- name: Propagate Secrets if existing
  include_role:
    name: common
    tasks_from: secrets
  tags:
    - reconfigure
    - secrets

- name: Copy GCP service account file
  copy:
    src: "{{ gcp_service_account_host_file }}"
    dest: "{{ gcp_service_account_target_file }}"
    mode: 0755
  tags:
    - reconfigure
    - secrets
  when:
    - cloud_provider == 'gcp'

- name: Reconfigure Sidekiq
  command: gitlab-ctl reconfigure
  register: result
  retries: 3
  until: result is success
  tags:
    - reconfigure

- name: Propagate Secrets if new
  include_role:
    name: common
    tasks_from: secrets
  vars:
    gitlab_secrets_reconfigure: true
  tags:
    - reconfigure
    - secrets

- name: Restart Sidekiq
  command: gitlab-ctl restart
  register: result
  retries: 2
  until: result is success
  tags:
    - reconfigure
    - restart

- name: Create skip-auto-reconfigure file
  file:
    path: /etc/gitlab/skip-auto-reconfigure
    state: touch
    mode: u=rw,g=r,o=r

- name: Run Custom Tasks
  block:
    - name: Check if Custom Tasks file exists
      stat:
        path: "{{ sidekiq_custom_tasks_file }}"
      register: sidekiq_custom_tasks_file_path
      delegate_to: localhost
      become: false

    - name: Run Custom Tasks
      include_tasks:
        file: "{{ sidekiq_custom_tasks_file }}"
        apply:
          tags: custom_tasks
      when: sidekiq_custom_tasks_file_path.stat.exists
  tags: custom_tasks
